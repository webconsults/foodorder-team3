var express = require('express');
var fs = require('fs');

var foodlist
fs.readFile('rescources/foodList.json', 'utf8', function(err, data) {
    if (err) throw err;
    foodlist = JSON.parse(data)
    console.log(foodlist)
});

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { foodlist: foodlist });
});

module.exports = router;

router.get('/order/:person/:dish/:number', function (req, res) {
  console.log(req.params)
    var person = req.params.person
    var dish = req.params.dish
    var number = req.params.number
    fs.writeFile('rescources/order.txt', person +","+ dish+","+number,'utf8',function(err, data){
        if(err) console.log(err);
    })

    res.render('order')
});


var orderId = 0;
router.post('/api', function(req, res) {
    console.log(req.body)
    var person = req.body.person
    var dish = req.body.dish
    var number = req.body.number
    fs.writeFile('rescources/order_' + orderId++ + '_' + new Date().toDateString() + '.txt', person +","+ dish+","+number,'utf8',function(err, data){
        if(err) console.log(err);
    })
    res.render('order')
});
